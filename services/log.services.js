import chalk from "chalk"
import dedent from "dedent-js"

const printError=(error)=>{
    console.log(chalk.bgRed('ERROR') + " " + error);
};
const printSuccess=(massage)=>{
    console.log(chalk.bgGreen('SUCCESS') + " " + massage);
};
const printHelp=()=>{
  console.log(
      dedent(
          chalk.bgCyan("HELP\n")+
      '-s [CITY] Для установки города\n' +
      '-h для вывода помощи\n' +
      '-t [API_KEY] для сохранения токена'

      ));
};
const printWeather =(res,icon)=>{
    console.log(
        dedent
            `${chalk.bgBlue("WEATHER")} Погола в городе  ${res.name}
            ${icon}  ${res.weather[0].description}
            Температура: ${res.main.temp}(Ощющаяеться как ${res.main.feels_like})
            Влажность: ${res.main.humidity}%
            Скорость ветра: ${res.wind.speed} m/s
            `
        )

};


export {printError,printSuccess,printHelp,printWeather};