import {homedir} from 'os';
import {join} from 'path';
import {promises} from 'fs';

const filePath=join(homedir(),'weather-data.json');

const TOCEN_DICTIONAGY={
    token: 'token',
    city: 'city'
};
const saveKeyValue=async (key,valuae)=>{ //сохранение в файле
    let data={};
    if(await isExist(filePath)){
        const file=await promises.readFile(filePath);
        data=JSON.parse(file);
    }
    data[key]=valuae;
     await promises.writeFile(filePath,JSON.stringify(data));
};
const getKeyValue=async (key)=>{    // Перезапись в файле
    if(await isExist(filePath)) {
        const file = await promises.readFile(filePath);
        const data = JSON.parse(file);
        return data[key];
    }return undefined
};
const isExist=async (path)=>{
    try {
        await promises.stat(path);
        return true
    }catch (e) {
        return false
    };

};

export {saveKeyValue,getKeyValue,TOCEN_DICTIONAGY};