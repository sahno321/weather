#!/usr/bin/env node

import { getArgs } from "./helpers/args.js"//импорт с args.js
import {printHelp, printSuccess, printError,printWeather} from "./services/log.services.js";
import {saveKeyValue,TOCEN_DICTIONAGY,getKeyValue} from "./services/storage.services.js";
import {getWeather,getIcon} from "./services/api.services.js";

const saveToken=async (token)=>{                     //сохранение токена
    if(!token.length){
        printError('Не передан токен')
        return;
    }
    try {
        await saveKeyValue(TOCEN_DICTIONAGY.token,token);
        printSuccess('Токен сохранен');
    } catch (e){
        printError(e.message);
    }
};

const saveCity =async (city)=>{                     //сохранение города
    if(!city.length){
        printError('Не передан город')
        return;
    }
    try {
        await saveKeyValue(TOCEN_DICTIONAGY.city,city);
        printSuccess('Город сохранен');
    } catch (e){
        printError(e.message);
    }
};

const getForcast=async ()=>{
    try {
        const city=process.env.City?? await getKeyValue(TOCEN_DICTIONAGY.city);
        const weather = await getWeather(city);
        printWeather(weather,getIcon(weather.weather[0].icon));
    }catch (e){
        if(e?.response?.status===404){
            printError("Неверно указан город");
        }else if(e?.response?.status===401){
            printError('Неверно указан токен');
        }else {
            printError(e.message);
        }
    }
};
const initCLI=()=>{
    const args = getArgs(process.argv)
    if(args.h){
        return printHelp()//Выводим Help
    }
    if(args.s){
        return saveCity(args.s)//сохранение  города
    }
    if(args.t){
       return saveToken(args.t); //сохраняем токен
    }
    return getForcast()//Вывести погоду
};
initCLI()